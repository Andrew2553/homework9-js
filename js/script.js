// 1. Створити новий елемент можна за допомогою document.createElement(tagName), де tagName - це назва нового тега, який ми хочемо створити. Наприклад, якщо ми хочемо створити <custom-tag>, використовуйте document.createElement('custom-tag').

// 2. Перший параметр функції insertAdjacentHTML визначає положення, де буде вставлено HTML-розмітку. Цей параметр має наступні можливі значення:

// 'beforebegin': Вставляє HTML-розмітку безпосередньо перед елементом, до якого викликається метод.
// 'afterbegin': Вставляє HTML-розмітку в початок (всередину) елемента, до якого викликається метод.
// 'beforeend': Вставляє HTML-розмітку в кінець (всередину) елемента, до якого викликається метод.
// 'afterend': Вставляє HTML-розмітку безпосередньо після елемента, до якого викликається метод.


// 3. Використанням методу remove():
//         приклад:
//         const element = document.getElementById('myElement');
// element.remove();

function renderList(array, parent = document.body) {
    const ul = document.createElement('ul');
    parent.appendChild(ul);
  
    array.forEach((item) => {
      const li = document.createElement('li');
      ul.appendChild(li);
  
      if (Array.isArray(item)) {
        const nestedUl = document.createElement('ul');
        li.appendChild(nestedUl);
        renderList(item, nestedUl);
      } else {
        li.textContent = item;
      }
    });
  }
  
  function renderTimer(seconds) {
    const timerDiv = document.createElement('div');
    document.body.appendChild(timerDiv);
  
    const intervalId = setInterval(() => {
      timerDiv.textContent = `Очищення сторінки через ${seconds} секунд`;
  
      seconds--;
  
      if (seconds < 0) {
        clearInterval(intervalId);
        document.body.innerHTML = '';
      }
    }, 1000);
  }
  
  const data = ["hello", "world", "Kiev", "Kharkiv", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"];
  renderList(data);
  
  // Відображення таймера перед очищенням сторінки
  renderTimer(3);
  